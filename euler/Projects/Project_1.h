#ifndef PROJECT1 
#define PROJECT1

#include "euler/IProject.h"

class Project_1 : public virtual IProject
{
public:
  typedef Project_1 Self;
  typedef Self * SPtr;
public:
  virtual std::string getName() const
  {
    return "Multiples of 3 and 5";
  }
  virtual std::string getDetails() const
  {
    return "If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.\n\
      Find the sum of all the multiples of 3 or 5 below 1000.\n";
  }
public:
  virtual void run()
  {
    size_t sum = 0;
    // Calculate stuff and return result as string.
    setOutput(toString<size_t>(sum));
  }
};

#endif /* PROJECT1 */