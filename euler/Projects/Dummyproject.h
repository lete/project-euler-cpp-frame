#ifndef DUMMYPROJECT
#define DUMMYPROJECT

#include "euler/IProject.h"

class DummyProject : public virtual IProject
{
public:
  typedef DummyProject Self;
  typedef Self * SPtr;
public:
  virtual std::string getName() const
  {
    return "";
  }
  virtual std::string getDetails() const
  {
    return "";
  }
public:
  virtual void run()
  {
   
  }
};

#endif /* DUMMYPROJECT*/