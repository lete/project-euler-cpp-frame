#ifndef ERADEN_EULER_IPROJECT
#define ERADEN_EULER_IPROJECT

#include "euler/utils.h"

class IProject
{
public:
  typedef IProject Self;
  typedef Self * SPtr;
public:
  virtual std::string getName() const = 0;
  virtual std::string getDetails() const = 0;
  virtual std::string getOutput() const
  {
    return m_output;
  }
public:
  virtual void setOutput( std::string const &rhs)
  {
    m_output = rhs;
  }
public:
  virtual void run() = 0;
protected:
  std::string m_output;
};

#endif /*ERADEN_EULER_IPROJECT*/