#ifndef EULER_UTILS
#define EULER_UTILS

#include <sstream>
#include <string>

/**
 * Anything to string converter using std::stringstream as utility.
 * Works for lots of things, but slow.
 * @param v the value to be converted
 * @param out the converted value
 */
template<typename T>
void toString(T const &v, std::string &out)
{
  std::ostringstream os;
  os << v;
  out = os.str();
}
/**
 * Anything to string converter using std::stringstream as utility.
 * Works for lots of things, but slow.
 */
template<typename T>
std::string toString(const T& t)
{
  std::string ret;
  toString(t, ret);
  return ret;
}

#endif /* EULER_UTILS */