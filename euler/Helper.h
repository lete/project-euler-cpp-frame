#ifndef ERADEN_EULER_HELPER
#define ERADEN_EULER_HELPER

#include <iostream>
#include <string>
#include <vector>
#include "euler/IProject.h"
#include "euler/projects.h"

class Helper
{
public:
  Helper()
  {
    run();
  }
  ~Helper()
  {
    for(size_t i = 0; i< m_projects.size(); ++i)
      delete m_projects.at(i);
  }
public:
  void printHeader() const
  {
    std::cout << "**************************************" << std::endl;
    std::cout << "*        Project Euler frame         *" << std::endl;
    std::cout << "**************************************" << std::endl;
    std::cout << std::endl;
  }
  void printMenu( bool verbose = false )
  {
    std::cout << "* Menu:                             *" << std::endl;
    for(size_t i=0; i<m_projects.size(); ++i)
    {
      std::cout << i << ". project: " << m_projects.at(i)->getName() << std::endl;
      if(verbose)
        std::cout << m_projects.at(i)->getDetails();
    }
    std::cout << "**************************************" << std::endl;
  }
private:
  void run()
  {
    setupMenu();
    printHeader();
    printMenu();
    getUserInput();
  }
  void setupMenu()
  {
    m_projects.push_back(IProject::SPtr(new Project_1));
  }
  void getUserInput()
  {
    while(true)
    {
      size_t projectNumber;
      std::cout << "Please state the number of project you want to execute (-1 to quit): ";
      std::cin >> projectNumber;
      if(projectNumber == -1)
        return;
      while(projectNumber < 0 || projectNumber > m_projects.size()-1)
      {
        std::cout << "Invalid number, please try again: ";
        std::cin >> projectNumber;
      }
      runProject(projectNumber);
    }
  }
  void runProject( size_t projectNumber )
  {
    m_projects.at(projectNumber)->run();
    std::cout << m_projects.at(projectNumber)->getOutput() << std::endl;
  }
private:
  std::vector<IProject::SPtr> m_projects;
};

#endif //ERADEN_EULER_HELPER